// C++ code of the Morse interpreter
//
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  // this is just a test to be deleted
  // switch led on and wait for half a second
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  // switch led off and wait for half a second
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}